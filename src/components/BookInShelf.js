import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Box,
  Button,
  Typography,
  Card,
  CardContent,
  CardActionArea,
  CardMedia,
  CardActions,
} from "@material-ui/core";
import { connect } from "react-redux";
import { addBookAction, removeBookAction } from "../api/action";
import { Link } from "react-router-dom";

import "../App.css";
//import "./Book.css";
const useStyles = makeStyles({
  root: {
    width: 150,
    margin: 10,
  },
  media: {
    height: 140,
  },
  links: {
    textDecoration: "none",
  },
});

const BookInShelf = ({ book, ...props }) => {
  const classes = useStyles();

  console.log(book);

  const urlSlug = (url) => {
    let newSlug;
    let urlArr = url.split("/");
    urlArr[urlArr.length - 1].length === 0
      ? (newSlug = urlArr[urlArr.length - 2])
      : (newSlug = urlArr[urlArr.length - 1]);
    return newSlug;
  };

  const checkSlug = () => {
    return book.slug !== undefined ? book.slug : urlSlug(book.url);
  };

  const addAuthors = (arr) => {
    console.log(book.authors);
    let namesArr = [];
    arr.map((item) => namesArr.push(item.name));
    return namesArr.join(", ");
  };

  const checkAuthor = () => {
    return book.author !== undefined ? book.author : addAuthors(book.authors);
  };

  return (
    <Box display="flex">
      <Card className={classes.root}>
        <Link to={"/book/" + checkSlug()} className={classes.links}>
          <CardActionArea>
            <CardMedia
              className={classes.media}
              image={book.cover}
              title="Cover image"
            />
            <CardContent>
              <Typography
                align="left"
                gutterBottom
                variant="h6"
                color="textPrimary"
                component="h2"
              >
                {checkAuthor()}
              </Typography>
              <Typography
                align="left"
                variant="body1"
                color="textPrimary"
                component="p"
              >
                {book.title}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Link>
        <CardActions>
          <Button
            size="small"
            color="primary"
            onClick={() => props.removeBook(book)}
          >
            Remove Book
          </Button>
        </CardActions>
      </Card>
    </Box>
  );
};

const mapStateToProps = (state) => {
  return {
    books: state.books,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addBook: (book) => dispatch(addBookAction(book)),
    removeBook: (book) => dispatch(removeBookAction(book)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BookInShelf);
