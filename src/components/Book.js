import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Favorite, FavoriteBorder, ArrowForward } from "@material-ui/icons";
import {
  Typography,
  Card,
  CardContent,
  CardMedia,
  CardActions,
  Tooltip,
} from "@material-ui/core";
import { connect } from "react-redux";
import { addBookAction, removeBookAction } from "../api/action";
import { Link } from "react-router-dom";

import "../App.css";
import "./Book.css";

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    margin: 10,
  },
  details: {
    display: "flex",
    flexDirection: "column",
  },
  content: {
    flex: "1 0 auto",
    textAlign: "left",
  },
  cover: {
    width: 151,
  },
  pos: {
    marginBottom: 12,
  },
  title: {
    fontSize: 14,
  },
  last: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "start",
    //alignSelf: 'end',
    margin: "0 0 0 auto",
  },
  like: {
    margin: 0,
    marginTop: "10px",
    border: "none",
    /*width: 30px;*/
    height: "30px",
    backgroundColor: "transparent",
    outline: "none",
  },
}));

const Book = ({ book, ...props }) => {
  const classes = useStyles();

  const checkBook = () => {
    let check = props.books.find((elem) => elem.title === book.title);
    return check;
  };
  //console.log(book);

  return (
    <Card className={classes.root}>
      <CardMedia
        className={classes.cover}
        image={book.cover}
        title="Live from space album cover"
      />
      <div className={classes.details}>
        <CardContent className={classes.content}>
          <Typography className={classes.title} color="textSecondary">
            Author
          </Typography>
          <Typography component="h6" variant="h6" gutterBottom>
            {book.author}
          </Typography>
          <Typography className={classes.title} color="textSecondary">
            Title
          </Typography>
          <Typography component="h5" variant="h5" gutterBottom>
            {book.title}
          </Typography>
          <Typography className={classes.title} color="textSecondary">
            Genre
          </Typography>
          <Typography component="p" variant="body1" color="textPrimary">
            {book.genre}
          </Typography>
        </CardContent>
      </div>
      <CardActions className={classes.last}>
        {checkBook() !== undefined ? (
          <Tooltip title="Remove" arrow>
            <button
              className="Book-like"
              onClick={() => props.removeBook(book)}
            >
              <Favorite color="secondary" />
            </button>
          </Tooltip>
        ) : (
          <Tooltip title="Add" arrow>
            <button className="Book-like" onClick={() => props.addBook(book)}>
              <FavoriteBorder color="secondary" />
            </button>
          </Tooltip>
        )}
        <Tooltip title="More" arrow>
          <Link to={"/book/" + book.slug} className={classes.like}>
            <ArrowForward style={{ color: "#000000" }} />
          </Link>
        </Tooltip>
      </CardActions>
    </Card>
  );
};

const mapStateToProps = (state) => {
  return {
    books: state.books,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addBook: (book) => dispatch(addBookAction(book)),
    removeBook: (book) => dispatch(removeBookAction(book)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Book);
