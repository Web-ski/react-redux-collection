import React from "react";
import { Link, useHistory, useLocation } from "react-router-dom";
import { connect } from "react-redux";
import { Favorite, ArrowBack } from "@material-ui/icons";
import { Tooltip } from "@material-ui/core";
import "./Header.css";

const Header = (props) => {
  let history = useHistory();
  let location = useLocation();

  function handleClick() {
    history.goBack();
  }

  function MainSite() {
    return (
      <Tooltip title="Go Back" arrow>
        <button onClick={handleClick} className="Book-like">
          <ArrowBack fontSize="large" style={{ color: "#ffffff" }} />
        </button>
      </Tooltip>
    );
  }

  return (
    <header className="App-header">
      <div className="navbar">
        <Link to="/" className="brand"></Link>
        <div className="nav-buttons">
          {location.pathname !== "/" && MainSite()}
          <Link
            to="/shelf"
            className={
              props.name !== undefined ? "GoTo-button nonactive" : "GoTo-button"
            }
          >
            <p>Your Shelf</p> <Favorite color="secondary" />{" "}
            <p>{props.number}</p>
          </Link>
        </div>
      </div>
    </header>
  );
};

const mapStateToProps = (state) => {
  const { number } = state;
  //console.log({ number });
  return { number };
};

export default connect(mapStateToProps)(Header);
