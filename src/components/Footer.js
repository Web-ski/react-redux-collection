import React from "react";
import "../App.css";
import "./Footer.css";
import { Typography, Link } from "@material-ui/core";

const Footer = () => {
  return (
    <footer className="App-footer">
      <Typography variant="body1">
        {"Picture "}
        <Link href="https://pixabay.com/pl/users/morningbirdphoto-129488/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1605416">
          {"MorningbirdPhoto "}
        </Link>
        {"from "}
        <Link href="https://pixabay.com/pl/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1605416">
          {"Pixabay "}
        </Link>
      </Typography>
      <Typography variant="body1">
        API source: https://wolnelektury.pl/api/
      </Typography>
      <Typography variant="body1" style={{ color: "#61dafb" }}>
        {"Author: "}
        <Link href="https://nieczuja-portfolio.pl" style={{ color: "#61dafb" }}>
          Paweł Nieczuja-Ostrowski
        </Link>
      </Typography>
    </footer>
  );
};

export default Footer;
