import React from "react";
import { connect } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import collectionAction from "../api/action";

import '../App.css';
import './Collection.css';

const Collection = ({ collection, number, ...props }) => {

  let { id } = useParams();

  const addUrl = (url) => {
    let newUrl = url.split("/");
    newUrl[newUrl.length - 1] !== false ? (newUrl = newUrl[newUrl.length - 2]) : (newUrl = newUrl[newUrl.length - 1]);
    return newUrl;
  }

  return (
    <Link to={addUrl(collection.url)} className="Collection-box">
      <div className="Collection-number"><span>{number}</span></div>
      <div className="Collection-description">
        <h4 className="Collection-title">{collection.title}</h4>
      </div>
    </Link>
  );
}

export default Collection;