import React from "react";
import "./BookDetails.css";
import { connect } from "react-redux";
import { addBookAction, removeBookAction } from "../api/action";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { Favorite, FavoriteBorder } from "@material-ui/icons";
import GetAppIcon from "@material-ui/icons/GetApp";
import { Button, Typography, Tooltip } from "@material-ui/core";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import { Link } from "react-router-dom";

const PrimaryButton = withStyles((theme) => ({
  root: {
    width: "150px",
    color: "#61dafb",
    backgroundColor: "#000000",
    "&:hover": {
      color: "#000000",
      backgroundColor: "#61dafb",
    },
  },
}))(Button);

const useStyles = makeStyles({
  library: {
    display: "block",
    marginTop: 10,
    cursor: "pointer",
  },
  collections: {
    color: "white",
  },
});

const BookDetails = ({ book, ...props }) => {
  const classes = useStyles();

  const checkBook = () => {
    let check = props.books.find((elem) => elem.title === book.title);
    return check;
  };

  function setAuthors(collection) {
    return (
      collection !== undefined &&
      collection.map((item, index) => (
        <Typography
          variant="h6"
          style={{ color: "#61dafb", marginBottom: "15px" }}
          key={index + item.toString()}
          className="Author"
        >
          {item.name}
        </Typography>
      ))
    );
  }

  function setInfo(collection) {
    return (
      collection !== undefined &&
      collection.map((item, index) => (
        <Typography variant="body2" key={index + item.toString()}>
          {item.name}
        </Typography>
      ))
    );
  }

  function addUrl(url) {
    let x = url.split("");
    x[x.length - 1] === "/" && x.pop();
    x = x.join("") + ".html";
    return x;
  }

  return (
    <>
      <article className="BookDetails">
        <div
          className="BookDetails-coverBox"
          style={{ backgroundImage: `url('${book.cover}')` }}
        >
          {/* <img className="Book-coverLarge" src={book.cover} /> */}
          <aside className="Title-triangle"></aside>
        </div>
        <div className="BookDetails-titles">
          {setAuthors(book.authors)}
          <Typography variant="h5" style={{ marginBottom: "15px" }}>
            {book.title}
          </Typography>
          {setInfo(book.genres)}
          {setInfo(book.epochs)}
          <Typography variant="body2">
            Language: {book.language === "pol" && "Polish"}
            {book.language === "eng" && "English"}
          </Typography>
        </div>

        <aside className="Book-actions">
          <Tooltip title="Go to Books Collections" arrow className={classes.library}>
            <Link to={"/collections"} className={classes.collections}>
              <LibraryBooksIcon></LibraryBooksIcon>
            </Link>
          </Tooltip>
          {checkBook() !== undefined ? (
            <Tooltip title="Remove" arrow>
              <button
                className="Book-like"
                onClick={() => props.removeBook(book)}
              >
                <Favorite color="secondary" />
              </button>
            </Tooltip>
          ) : (
            <Tooltip title="Add" arrow>
              <button className="Book-like" onClick={() => props.addBook(book)}>
                <FavoriteBorder color="secondary" />
              </button>
            </Tooltip>
          )}
        </aside>
      </article>
      <article className="BookDetails-buttons">
        <a
          href={book.url && addUrl(book.url)}
          className="btn-start"
          target="_blank"
          rel="noreferrer"
        >
          <PrimaryButton variant="contained" size="large">
            Online book
          </PrimaryButton>
        </a>
        <a href={book.pdf} className="btn-start" download target="_blank" rel="noreferrer">
          <PrimaryButton
            variant="outlined"
            size="large"
            startIcon={<GetAppIcon />}
          >
            PDF
          </PrimaryButton>
        </a>
      </article>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    books: state.books,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addBook: (book) => dispatch(addBookAction(book)),
    removeBook: (book) => dispatch(removeBookAction(book)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BookDetails);
