import React, { useState, useEffect } from "react";
import { useParams } from 'react-router-dom';
import './ChoosenBook.css';
import { CircularProgress, Container } from '@material-ui/core';
import Header from '../components/Header';
import BookDetails from '../components/BookDetails';
import axios from 'axios';

const ChoosenBook = () => {
  const [book, setBook] = useState([]);
  let { id } = useParams();

  const url = `https://wolnelektury.pl/api/books/${id}/`;

  useEffect(() => {
    axios.get(url)
      .then(res => {
        //console.log(res);
        console.log(res.data);
        setBook(res.data);
      })
  }, []);

  return (
    <>
      <Header />
      <Container maxWidth="sm">
        <section className="ChoosenBook">
          {
            book !== undefined ?
              <BookDetails book={book} />
              : <CircularProgress />
          }
        </section>
      </Container>
    </>
  );
}

export default ChoosenBook;