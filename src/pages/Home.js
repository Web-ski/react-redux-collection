import React from "react";
import { connect } from "react-redux";
import "../App.css";
import "./Pages.css";
import { CircularProgress, Container } from "@material-ui/core";
import Header from "../components/Header";
import Collection from "../components/Collection";

const Home = (props) => {
  console.log(props);
  return (
    <>
      <Header />
      <Container maxWidth="sm">
        <section className="Home">
          <h1>Book Collections</h1>
          {props.collections !== undefined ? (
            props.collections.map((item, index) => (
              <Collection
                key={index + item.toString()}
                collection={item}
                number={index + 1}
              />
            ))
          ) : (
            <CircularProgress />
          )}
        </section>
      </Container>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    collections: state.collections,
  };
};

export default connect(mapStateToProps)(Home);
