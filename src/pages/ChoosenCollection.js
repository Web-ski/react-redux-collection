import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import "../App.css";
import "./Pages.css";
import { CircularProgress, Container } from "@material-ui/core";
import Header from "../components/Header";
import axios from "axios";
import Book from "../components/Book";

const ChoosenCollection = () => {
  const [collection, setCollection] = useState();
  let { id } = useParams();
  console.log({ id });

  const url = `https://wolnelektury.pl/api/collections/${id}/`;

  useEffect(() => {
    axios.get(url).then((res) => {
      //console.log(res.data);
      setCollection(res.data);
    });
  }, []);

  return (
    <>
      <Header />
      <Container maxWidth="sm">
        <section className="ChoosenCollection">
          <h1>Collection</h1>
          <h2>
            {collection !== undefined ? (
              `"${collection.title}"`
            ) : (
                <CircularProgress />
              )}
          </h2>
          {collection !== undefined ? (
            collection.books.map((item, index) => (
              <Book key={index + item.toString()} book={item} />
            ))
          ) : (
              <CircularProgress />
            )}
        </section>
      </Container>
    </>
  );
};

export default ChoosenCollection;
