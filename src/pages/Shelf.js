import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import "../App.css";
import { makeStyles } from "@material-ui/core/styles";
import { Box, Container, Tooltip } from "@material-ui/core";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import Header from "../components/Header";
import BookInShelf from "../components/BookInShelf";

const useStyles = makeStyles({
  library: {
    position: "absolute",
    right: 10,
    top: 10,
    cursor: "pointer",
  },
  collections: {
    color: "white",
  },
});

const Shelf = (props) => {
  const classes = useStyles();

  return (
    <>
      <Header name="shelf" />
      <Container maxWidth="sm">
        <section className="Shelf">
          <h1>Your Shelf</h1>
          <Tooltip
            title="Go to Books Collections"
            arrow
            className={classes.library}
          >
            <Link to={"/collections"} className={classes.collections}>
              <LibraryBooksIcon></LibraryBooksIcon>
            </Link>
          </Tooltip>
          <Box display="flex" flexDirection="row" flexWrap="wrap">
            {props.books !== undefined &&
              props.books.map((item, index) => (
                <BookInShelf key={index + item.toString()} book={item} />
              ))}
          </Box>
        </section>
      </Container>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    books: state.books,
  };
};

export default connect(mapStateToProps)(Shelf);
