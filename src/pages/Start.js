import React from "react";
import "../App.css";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { Container, Button } from "@material-ui/core";
import Footer from "../components/Footer";

const PrimaryButton = withStyles((theme) => ({
  root: {
    color: "#61dafb",
    backgroundColor: "#000000",
    "&:hover": {
      color: "#000000",
      backgroundColor: "#61dafb",
    },
  },
}))(Button);

const Start = (props) => {
  return (
    <>
      <main className="App-main">
        <Container maxWidth="sm" style={{ backgroundColor: "#282c34" }}>
          <div className="triangle triangle-top-left"></div>
          <div className="triangle triangle-top-right"></div>
          <div className="triangle triangle-bottom-left"></div>
          <div className="triangle triangle-bottom-right"></div>
          <h2>Hi, You are at the</h2>
          <h1>FreeBook Collection</h1>
          <h4 className="App-title">
            The Project of Books Storage created with React and Redux with
            Material UI
          </h4>
          <p className="App-subtitle">Based on https://wolnelektury.pl/api/</p>
          <Link to="/collections" className="btn-start">
            <PrimaryButton variant="contained" size="large">
              Start
            </PrimaryButton>
          </Link>
        </Container>
      </main>
      <Footer />
    </>
  );
};

export default Start;
