import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  BrowserRouter,
} from "react-router-dom";
import axios from "axios";
import { connect } from "react-redux";
import { addCollectionsAction } from "./api/action";
import "./App.css";
import Start from "./pages/Start";
import Home from "./pages/Home";
import Shelf from "./pages/Shelf";
import ChoosenCollection from "./pages/ChoosenCollection";
import ChoosenBook from "./pages/ChoosenBook";

const App = (props) => {
  const [collections, setCollections] = useState();
  const url = "https://wolnelektury.pl/api/collections/";

  useEffect(() => {
    //console.log(collections);
    axios
      .get(url)
      .then((res) => {
        setCollections(res.data);
        //props.addCollections(res.data);
      })
      .catch((error) => console.log(error));
  }, []);

  props.addCollections(collections);

  return (
    <BrowserRouter basename="/react-redux-collection">
      <div className="App">
        <aside className="App-bg"></aside>
        <Switch>
          <Route path="/book/:id">
            <ChoosenBook />
          </Route>
          <Route exact path="/shelf">
            <Shelf />
          </Route>
          <Route path="/collections">
            <Home />
          </Route>
          <Route path="/:id">
            <ChoosenCollection />
          </Route>
          <Route path="/">
            <Start />
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    addCollections: (data) => dispatch(addCollectionsAction(data)),
  };
};

export default connect(null, mapDispatchToProps)(App);
