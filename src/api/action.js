const addCollectionsAction = (data) => ({
  type: "ADD_COLLECTIONS",
  payload: {
    addedCollections: data,
  },
});

const addBookAction = (data) => ({
  type: "ADD_BOOK",
  payload: {
    addedBook: data,
  },
});

const removeBookAction = (data) => ({
  type: "REMOVE_BOOK",
  payload: {
    removedBook: data,
  },
});

export { addCollectionsAction, addBookAction, removeBookAction };
