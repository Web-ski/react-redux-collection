const initialState = {
  number: 0,
  books: [],
};

const bookReducer = (state = initialState, { payload, type }) => {
  switch (type) {
    case "ADD_COLLECTIONS": {
      return {
        ...state,
        collections: payload.addedCollections,
      };
    }
    case "ADD_BOOK": {
      return {
        ...state,
        books: [...state.books, payload.addedBook],
        number: state.books.length + 1,
      };
    }
    case "REMOVE_BOOK": {
      return {
        ...state,
        books: state.books.filter(
          (item) => item.title !== payload.removedBook.title
        ),
        number: state.books.length - 1,
      };
    }
    default:
      return state;
  }
};

export default bookReducer;
